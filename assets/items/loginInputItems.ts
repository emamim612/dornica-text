export default [
    {
        label: 'ایمیل',
        name: 'email',
        placeholder: 'example@mail.com',
        icon: '/img/icons/email.svg',
        type: 'text',
        ComponentName: 'BaseInput',
    },
    {
        label: 'رمز عبور',
        name: 'password',
        placeholder: 'حداقل 8 کاراکتر',
        icon: '/img/icons/password.svg',
        type: 'password',
        ComponentName: 'BaseInput',
    },
]