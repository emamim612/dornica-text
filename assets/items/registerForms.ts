export default [
    {
        title: 'اطلاعات فردی',
        formItems:  [
            {
                label: 'نام و نام خانوادگی',
                name: 'full_name',
                placeholder: 'محمد حسین رحمتی',
                icon: '/img/icons/email.svg',
                type: 'text',
                ComponentName: 'BaseInput',
                col: 'full-width'
            },
            {
                label: 'کد ملی',
                name: 'national_code',
                placeholder: '208-1235-456',
                icon: '/img/icons/password.svg',
                type: 'text',
                ComponentName: 'BaseInput',
                col: 'full-width'
            },
            {
                label: 'تاریخ تولد',
                name: 'birthday',
                placeholder: '1370/06/31',
                icon: '/img/icons/password.svg',
                type: 'text',
                ComponentName: 'BaseInput',
                col: 'full-width'
            },
        ],
    },
    {
        title: 'اطلاعات ارتباطی',
        formItems: [],
    },
    {
        title: 'اطلاعات مکانی',
        formItems: []
    },
]