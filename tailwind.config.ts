import type { Config } from 'tailwindcss'
import defaultTheme from 'tailwindcss/defaultTheme'
export default <Partial<Config>>{
    content: [
        "./components/**/*.{js,vue,ts}",
        "./layouts/**/*.vue",
        "./pages/**/*.vue",
        "./plugins/**/*.{js,ts}",
        "./nuxt.config.{js,ts}",
        "./app.vue",
    ],
    theme: {
        extend: {
            colors: ({ colors }) => ({
                cyan: {...colors.cyan, '50': '#e8f4ff'},
                blue: {...colors.blue, '500': '#388AEA', contrastText: '#fff'},
                gray: {...colors.gray, '200': '#D6D6D6'},
                slate: {...colors.slate, '400': '#9CC4F2'},
            }),
            spacing: {
                3: '8px',
                4: '11px',
                5: '12px',
                6: '18px',
                7: '21px',
                8: '24px',
                10: '32px',
                16: '40px',
                19: '51px',
                20: '53px',
                22: '64px',
                24: '77px',
                28: '85px',
                32: '94px'
            },
            borderWidth: {
                3: '3px',
            },
            borderRadius: {
                xl: '16px',
                '3xl': '51px' 
            },
            fontSize: {
                xs: ['16px', { lineHeight: '24.8px' }],
                sm: ['20px', { lineHeight: '31px' }],
                'xl': ['36px', { lineHeight: '55.8px' }],
                '2xl': ['40px', { lineHeight: '62px' }],
            }
        }
    }
}